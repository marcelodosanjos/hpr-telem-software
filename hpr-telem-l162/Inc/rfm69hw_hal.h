#ifndef RFM69HW_HAL_H
#define RFM69HW_HAL_H

#define FIELD_BOARD     1
#define DEVEL_BOARD     2

#define STM32L1_MCU		1
#define STM32F4_MCU		2

/*Define here your firmware target*/
#define TARGET_BOARD FIELD_BOARD
//#define TARGET_BOARD DEVEL_BOARD

/*Define here your mcu series*/
#define MCU_SERIES	 STM32L1_MCU
//#define MCU_SERIES	 STM32F4_MCU

//typedef SPI_HandleTypeDef *RFM_SPI_HandleP;

#ifndef MCU_SERIES
        #error DEFINE A MCU SERIES FOR PROPER HAL INCLUSION
#elif MCU_SERIES == 1
        #include "stm32l1xx_hal.h"
        #include "stm32l1xx_hal_gpio.h"
        #include "stm32l1xx_hal_rcc.h"
        #include "stm32l1xx_hal_spi.h"
#elif MCU_SERIES == 2
        #include "stm32f4xx_hal.h"
        #include "stm32f4xx_hal_gpio.h"
        #include "stm32f4xx_hal_rcc.h"
        #include "stm32f4xx_hal_spi.h"
#endif

#ifndef TARGET_BOARD
        #error DEFINE A TARGET FOR YOUR FIRMWARE
#elif TARGET_BOARD == 1

        extern SPI_HandleTypeDef hspi1; //hspi3set to hspi3 for lsfboard
        #define RFM_NSS_PIN_PORTx       GPIOB //set to GPIOB for lsbboard
        #define RFM_SPI_PORTx           GPIOA //set to GPIOA for lsfboard
        #define RFM_SPI_SCK_PIN_NO      GPIO_PIN_5 //set to GPIO_PIN_5 for lsfboard
        #define RFM_SPI_MISO_PIN_NO     GPIO_PIN_6 //set to GPIO_PIN_6 for lsfboard
        #define RFM_SPI_MOSI_PIN_NO     GPIO_PIN_7 //set to GPIO_PIN for lsfboard
        #define RFM_SPI_NSS_PIN_NO      GPIO_PIN_0 //set to GPIO_PIN_2 for lsfboard

#elif TARGET_BOARD == 2

        extern SPI_HandleTypeDef hspi3; //set to hspi3 for devboard
//    extern RFM_SPI_HandleP testrrrr = &hspi3;
        #define RFM_NSS_PIN_PORTx       GPIOD //set to GPIOD for devboard
        #define RFM_SPI_PORTx           GPIOC //set to GPIOC for devboard
        #define RFM_SPI_SCK_PIN_NO      GPIO_PIN_10 //set to GPIO_PIN_10 for devboard
        #define RFM_SPI_MISO_PIN_NO     GPIO_PIN_11 //set to GPIO_PIN_6 for devboard
        #define RFM_SPI_MOSI_PIN_NO     GPIO_PIN_12 //set to GPIO_PIN for devboard
        #define RFM_SPI_NSS_PIN_NO      GPIO_PIN_2 //set to GPIO_PIN_2 for devboard

#else
#error SOMETHING TOTALY WRONG
#endif

/*Define power controlling pins*/
#define RFM_POWER_PORTx         GPIOA
#define RFM_POWER_PIN           GPIO_PIN_4


#endif
