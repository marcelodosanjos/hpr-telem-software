/*Library for driving a RFM69HW rf module*/
#ifndef RFM69HW_REGS_H
#define RFM69HW_REGS_H

/* Common configuration registers */
#define REGFIFO_REG             0x00
#define REGOPMODE_REG           0x01 //MULTIPLE MASKS NEEDED
#define REGDATAMODULE_REG       0x02 //-#-
#define REGBITRATEMSB_REG       0x03 //-#-
#define REGBITRATELSB_REG       0x04 //-#-
#define REGFDEVMSB_REG          0x05 //-#-
#define REGFDEVLSB_REG          0x06 //-#-
#define REGFRFMSB_REG           0x07
#define REGFRMMID_REG           0x08
#define REGFRFLSB_REG           0x09
#define REGOSC1_REG             0x0A //-#-
#define REGAFCCTRL_REG          0x0B //-#-
#define REGRESERVED_REG         0x0C //UNUSED
#define REGLISTEN1_REG          0x0D //-#-
#define REGLISTEN2_REG          0x0E
#define REGLISTEN3_REG          0x0F
#define REGVERSION_REG          0x10

/* Transmitter Registers */
#define REGPALEVEL_REG          0x11 //-#-
#define REGPARAMP_REG           0x12 //-#-
#define REGOCP_REG              0x13 //-#-

/* Receiver Registers */
#define RESERVED14_REG          0x14 //UNUSED
#define RESERVED15_REG          0x15 //UNUSED
#define RESERVED16_REG          0x16 //UNUSED
#define RESERVED17_REG          0x17 //UNUSED
#define REGLNA_REG              0x18 //-#-
#define REGRxBW_REG             0x19 //-#-
#define REGAFCBW_REG            0x1A //-#-
#define REGOOKPEAK_REG          0x1B //-#-
#define REGOOKAVG_REG           0x1C //-#-
#define REGOOKFIx_REG           0x1D //-#-
#define REGAFCFEI_REG           0x1E //-#-
#define REGAFCMSB_REG           0x1F //-#-
#define REGAFCLSB_REG           0x20 //-#-
#define REGFEIMSB_REG           0x21
#define REGFEILSB_REG           0x22
#define REGRSSICONFIG_REG       0x23 //-#-
#define REGRSSIVALUE_REG        0x24

/* IRQ and PIN mapping registers */
#define REGDIOMAPPING1_REG      0x25 //-#-
#define REGDIOMAPPING2_REG      0x26 //-#-
#define REGIRQFLAGS1_REG        0x27 //-#-
#define REGIRQFLAGS2_REG        0x28 //-#-
#define REGRSSITHRESH_REG       0x29
#define RExRxTIMEOUT1_REG       0x2A
#define REGRxTIMEOUT2_REG       0x2B

/* Packet engine registers */
#define REGPREAMBLEMSB_REG      0x2C
#define REGPREAMBLELSB_REG      0x2D
#define REGSYNCCONFIG_REG       0x2E //-#-
#define REGSYNCVALUE1_REG       0x2F
#define REGSYNCVALUE2_REG       0x30
#define REGSYNCVALUE3_REG       0x31
#define REGSYNCVALUE4_REG       0x32
#define REGSYNCVALUE5_REG       0x33
#define REGSYNCVALUE6_REG       0x34
#define REGSYNCVALUE7_REG       0x35
#define REGSYNCVALUE8_REG       0x36
#define REGPACKETCONFIG1_REG    0x37 //-#-
#define REGPAYLOADLENGTH_REG    0x38
#define REGNODEADRS_REG	        0x39
#define REGBROADCASTADRS_REG    0x3A
#define REGAUTOMODES_REG        0x3B //-#-
#define REGFIFOTHRESH_REG       0x3C //-#-
#define REGPACKETCONFIG2_REG    0x3D //-#-
#define REGAESKEY1_REG          0x3E
#define REGAESKEY2_REG          0x3F
#define REGAESKEY3_REG          0x40
#define REGAESKEY4_REG          0x41
#define REGAESKEY5_REG          0x42
#define REGAESKEY6_REG          0x43
#define REGAESKEY7_REG          0x44
#define REGAESKEY8_REG          0x45
#define REGAESKEY9_REG          0x46
#define REGAESKEY10_REG         0x47
#define REGAESKEY11_REG         0x48
#define REGAESKEY12_REG         0x49
#define REGAESKEY13_REG         0x4A
#define REGAESKEY14_REG         0x4B
#define REGAESKEY15_REG         0x4C
#define REGAESKEY16_REG         0x4D

/* Temperature sensor registers */
#define REGTEMP1_REG            0x4E //-#-
#define REGTEMP2_REG            0x4F

/* Test registers */
#define REGTESTLNA_REG          0x58
#define REGTESTPA1_REG          0x5A
#define REGTESTPA2_REG          0x5C
#define REGTESTDAGC_REG         0x6F
#define REGTESTAFC_REG          0x71

#endif
