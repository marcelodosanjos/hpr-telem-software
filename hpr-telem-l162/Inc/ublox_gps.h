/*
 * ublox_gps.h
 *
 *  Created on: Apr 10, 2017
 *      Author: drid
 *      Based on UPSat ADCS code
 */

#ifndef GPS_H_
#define GPS_H_

#include "gps.h"
#include "stm32l1xx_hal.h"

void init_gps_uart();
void HAL_GPS_Alarm_Handler(_gps_state *gps_state_value);
void HAL_GPS_UART_IRQHandler(UART_HandleTypeDef *huart);
void UART_GPS_Receive_IT(UART_HandleTypeDef *huart);

#endif /* GPS_H_ */
