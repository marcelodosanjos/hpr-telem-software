/*
 * hpr_lsm9ds0_hal.c
 *
 *  Created on: Apr 16, 2017
 *      Author: ariolis
 */

#include "hpr_lsm9ds0_hal.h"

//TEMPORAL
extern uint16_t imu_test_count;

lsm9ds0_sensor_status init_hpr_lsm9ds0( void){

	/* Initialize LSM9DS0 for magnetometer */
	uint8_t i2c_temp[2];

	HAL_I2C_Mem_Read(&hi2c1, (XM_ADDR << 1), WHO_AM_I_XM | LSM9DS0_MASK, 1, i2c_temp, 1, LSM9DS0_TIMEOUT);
	if (i2c_temp[0] != XM_ID) {
		return DEVICE_ERROR;
	}

	// 0x1F --> 0b00000000
	// 0x20 --> 0b00001000
	// 0x21 --> 0b00000000
	// 0x22 --> 0b00000000
	// 0x23 --> 0b00000000
	// 0x24 --> 0b01110100, 100Hz Data rate, TEMP Enable
	// 0x25 --> 0b00000000, 2Gauss
	// 0x26 --> 0b00000000
	uint8_t XM_CTRreg[8] = { 0x00, 0x08, 0x00, 0x00, 0x0, 0x74, 0x00, 0x00 };

	HAL_I2C_Mem_Write(&hi2c1, (XM_ADDR << 1),XM_CTR_REG | LSM9DS0_MASK, 1, XM_CTRreg, 8, LSM9DS0_TIMEOUT);


	/* Initialize LSM9DS0 for gyro */

	HAL_I2C_Mem_Read(&hi2c1, (GYRO_ADDR << 1), WHO_AM_I_G | LSM9DS0_MASK, 1, i2c_temp, 1, LSM9DS0_TIMEOUT);
	if (i2c_temp[0] != GYRO_ID) {
		return DEVICE_ERROR;
	}

	// 0x20 --> 0b01111111, 190Hz, LPF Cut off 70Hz
	// 0x21 --> 0b00100101, HPF cut off 0.45Hz
	// 0x22 --> 0b00000000
	// 0x23 --> 0b10000000, 245 DPS
	// 0x24 --> 0b00000000
	uint8_t GyroCTRreg[5] = { 0x7F, 0x25, 0x00, 0x80, 0x00 };
	HAL_I2C_Mem_Write(&hi2c1, (GYRO_ADDR << 1), CTRL_REG1_G | LSM9DS0_MASK, 1, GyroCTRreg, 5, LSM9DS0_TIMEOUT);


	/*No calibration because only raw data will be saved*/

	/* Initialize temperature */
	uint8_t buffer;
	memRead(TEMP_ADDR, CTRL_REG5_XM, &buffer, 1);

	buffer |= (1 << 7);
	memWrite(TEMP_ADDR, CTRL_REG5_XM, &buffer, 1);



	/*Initialize accelerometer */

	buffer =0 ;

	memRead(ACC_ADDR, WHO_AM_I_A, &buffer, 1);

	// Set control register for bypass
	buffer = 0x00;
	memWrite(ACC_ADDR, CTRL_REG0_XM, &buffer, 1);

	// Enable XYZ, rate 100Hz
	buffer = ACC_X_EN | ACC_Y_EN | ACC_Z_EN | LSM9DS0_ACCELDATARATE_100HZ | ACC_BDU;
	memWrite(ACC_ADDR, CTRL_REG1_XM, &buffer, 1);

	// Set range
	buffer = A_ABW_773 | A_AST_NORMAL | LSM9DS0_ACCELRANGE_16G;
	memWrite(ACC_ADDR, CTRL_REG2_XM, &buffer, 1);

	return DEVICE_NORMAL;//TODO: add error handling

}


void update_hpr_lsm9ds0_xm(IMU_DataStoreTypeDef *imu_data_bufer){

	int16_t data_buffer_raw[3];/*buffer for i2c reads*/

	/* Update values for lsm9ds0 magnetometer*/
	HAL_I2C_Mem_Read(&hi2c1, (XM_ADDR << 1), XM_VAL | LSM9DS0_MASK, 1, (uint8_t *) data_buffer_raw, 6, LSM9DS0_TIMEOUT);
	int16_t xm_raw_x = data_buffer_raw[0];
	int16_t xm_raw_y = data_buffer_raw[1];
	int16_t xm_raw_z = data_buffer_raw[2];
	/* store to imu data buffer */
	imu_data_bufer->magnetometer_x_axis_raw = xm_raw_x;
	imu_data_bufer->magnetometer_y_axis_raw = xm_raw_y;
	imu_data_bufer->magnetometer_z_axis_raw = xm_raw_z;

}

void update_hpr_lsm9ds0_gyro(IMU_DataStoreTypeDef *imu_data_bufer){

	int16_t data_buffer_raw[3];/*buffer for i2c reads*/

	/* Update values for lsm9ds0 gyroscope */
	HAL_I2C_Mem_Read(&hi2c1, (GYRO_ADDR << 1), GYRO_VAL | LSM9DS0_MASK, 1, (uint8_t *) data_buffer_raw, 6, LSM9DS0_TIMEOUT);
	int16_t gyr_raw_x = data_buffer_raw[0];
	int16_t gyr_raw_y = data_buffer_raw[1];
	int16_t gyr_raw_z = data_buffer_raw[2];

	imu_data_bufer->gyroscope_x_axis_raw = gyr_raw_x;
	imu_data_bufer->gyroscope_y_axis_raw = gyr_raw_y;
	imu_data_bufer->gyroscope_z_axis_raw = gyr_raw_z;

	//TEMPORAL
	//imu_data_bufer->gyroscope_x_axis_raw = 0xBBB1;
	//imu_data_bufer->gyroscope_y_axis_raw = 0xBBB2;
	//imu_data_bufer->gyroscope_z_axis_raw = 0xBBB3;



}

void update_hpr_lsm9ds0_temp(IMU_DataStoreTypeDef *imu_data_bufer){

	/* Read temperature */
	uint8_t temp_buffer[2];
    memRead(TEMP_ADDR, OUT_TEMP_L_XM, temp_buffer, 2);
    int16_t temp_raw = (temp_buffer[1] << 8) | temp_buffer[0];

    imu_data_bufer->temperature_raw = temp_raw;

}

void update_hpr_lsm9ds0_acc(IMU_DataStoreTypeDef *imu_data_bufer){

    /* Update accelerometer values */
    uint8_t acc_buffer[6];
	memRead(ACC_ADDR, OUT_X_L_A, acc_buffer, 6);
	int16_t acc_raw_x = acc_buffer[0] | (acc_buffer[1]<<8);
	int16_t acc_raw_y = acc_buffer[2] | (acc_buffer[3]<<8);
	int16_t acc_raw_z = acc_buffer[4] | (acc_buffer[5]<<8);

	imu_data_bufer->accelerometer_x_axis_raw = acc_raw_x;
	imu_data_bufer->accelerometer_y_axis_raw = acc_raw_y;
	imu_data_bufer->accelerometer_z_axis_raw = acc_raw_z;

	//TEMPORAL
	//imu_data_bufer->accelerometer_x_axis_raw = 0xBBB4;
	//imu_data_bufer->accelerometer_y_axis_raw = 0xBBB5;
	//imu_data_bufer->accelerometer_z_axis_raw = imu_test_count;
	//imu_test_count++;

}
