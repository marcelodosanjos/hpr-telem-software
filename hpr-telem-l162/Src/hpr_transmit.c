/*
 * hpr_transmit.c
 *
 *  Created on: Apr 10, 2017
 *      Author: ariolis
 */
#include "hpr_transmit.h"
#include "stm32l1xx_hal.h"
#include "string.h"

extern UART_HandleTypeDef huart2;
extern uint8_t rfmtx_id;
extern uint8_t rfm_cnt;
extern uint32_t rfm_tx_count;

void HPR_EspBlockTransmit(HPR_RingBufferTypeDef *ring_buffer, uint8_t *tx_buffer){

	/*Transmit a block of ESP8266_TXBUFFERSIZE to esp8266 over uart2 with dma transfer.
	 * esp8266 expects a $ for start flag and * for end flag with a maximum packet length of 255 bytes*/



	//	//start iterate data from hpr ring buffer and packetize them into tx buffer
	//	for (int entry_index = 0; entry_index < RINGBUFFER_LENGTH; ++entry_index) {
	//
	//
	//
	//	}

	/*or just block copy*/
	size_t copy_length = ESP8266_TXBUFFERSIZE-2;
	memcpy( &tx_buffer[1], ring_buffer, copy_length);
	tx_buffer[0] = (uint8_t)ESP8266_START_FLAG;
	tx_buffer[254] = (uint8_t)ESP8266_STOP_FLAG;

	if(HAL_UART_Transmit_DMA(&huart2, (uint8_t*)tx_buffer, ESP8266_TXBUFFERSIZE)!= HAL_OK)
	{
		/* Transfer error in transmission process */
		//handle this.
	}

}


void HPR_RfmGpsTransmit(HPR_RingBufferTypeDef *ring_buffer, uint8_t *rfm_data){

    uint8_t pos=1;
    uint32_t tick = HAL_GetTick();

	memcpy(rfm_data+pos, &rfmtx_id, 6); pos+=6;
	memcpy(rfm_data+pos, &rfm_cnt, 1); pos+=1; //id
	memcpy(rfm_data+pos, &tick, 4); pos+=4; //ticks
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].battery_voltage), 1); pos+=1;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].temperature_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_x_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_y_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].accelerometer_z_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_x_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_y_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].imu_datastore[ring_buffer->index].gyroscope_z_axis_raw), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].bme_datastore.pressure), 4); pos+=4;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].bme_datastore.temperature), 4); pos+=4;
    memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].bme_datastore.humidity), 4); pos+=4;
    memcpy(rfm_data+pos, (uint32_t)ring_buffer->data_log[ring_buffer->index].gps_datastore.lat, 4); pos+=4;
	memcpy(rfm_data+pos, (uint32_t)ring_buffer->data_log[ring_buffer->index].gps_datastore.lon, 4); pos+=4;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].gps_datastore.alt), 2); pos+=2;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].gps_datastore.d3fix), 1); pos+=1;
	memcpy(rfm_data+pos, &(ring_buffer->data_log[ring_buffer->index].gps_datastore.fix_timestamp), 4); pos+=4;
	memcpy(rfm_data+pos, &rfm_tx_count, 4); pos+=4;
	rfm_tx_count++;


//	for(uint8_t te=0;te<2;te++){
	for(uint8_t te=0;te<7;te++){
	    uint8_t re= 0;
	    re = (uint8_t)random();
	    memcpy( rfm_data+pos+te, &re, 1 );
//	    memcpy( rfm_data+te+pos, &re, 4);
//	    pos+=4;
	}

	WriteModuleRegisterN(0x80, rfm_data, 66);
}

uint8_t HLDLC_frame(uint8_t *input_buffer, uint8_t *output_buffer, uint16_t *buffer_size) {


	uint16_t buffer_index = 2;

	for (uint16_t i = 0; i < *buffer_size; i++) {
		if (i == 0) {
			output_buffer[0] = ESP8266_START_FLAG;
			output_buffer[1] = input_buffer[0];
		}
		else if (i == (*buffer_size) - 1) {

			if (input_buffer[i] == ESP8266_START_FLAG) {
				output_buffer[buffer_index++] = ESP8266_STOP_FLAG;
				output_buffer[buffer_index++] = 0x5E;
			}
			else if (input_buffer[i] == ESP8266_STOP_FLAG) {
				output_buffer[buffer_index++] = ESP8266_STOP_FLAG;
				output_buffer[buffer_index++] = 0x5D;
			}
			else {
				output_buffer[buffer_index++] = input_buffer[i];
			}

			output_buffer[buffer_index++] = ESP8266_START_FLAG;
			*buffer_size = buffer_index;
			return 0xff;

		}
		else if (input_buffer[i] == ESP8266_START_FLAG) {
			output_buffer[buffer_index++] = ESP8266_STOP_FLAG;
			output_buffer[buffer_index++] = 0x5E;
		}
		else if (input_buffer[i] == ESP8266_STOP_FLAG) {
			output_buffer[buffer_index++] = ESP8266_STOP_FLAG;
			output_buffer[buffer_index++] = 0x5D;
		}
		else {
			output_buffer[buffer_index++] = input_buffer[i];
		}

	}

	return 0;
}

