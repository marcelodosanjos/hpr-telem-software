/*
 * lsm9ds0.c
 *
 *  Created on: Apr 3, 2017
 *      Author: drid
 *      based on https://github.com/librespacefoundation/upsat-adcs-software
 */
#include "stm32l1xx_hal.h"
#include "stm32l1xx_hal_i2c.h"
#include "lsm9ds0.h"


extern I2C_HandleTypeDef hi2c1;

static const float XM_SCALE[] = { 0.932797, -0.0297068, -0.0197907, -0.0297068,
		0.986354, -0.0150793, -0.0197907, -0.0150793, 0.916263 };
static const float XM_OFFSET[] = { -1165.66, 194.66, 629.292 };

//float norm(float x1, float x2, float x3) {
//	return sqrtf(x1 * x1 + x2 * x2 + x3 * x3);
//}
/**
 * Wrapper for I2C read
 */
lsm9ds0_sensor_status memRead(uint16_t dev_id, uint16_t reg_addr,
		uint8_t *reg_data, uint8_t count) {
	HAL_StatusTypeDef status = HAL_OK;
	while (HAL_I2C_IsDeviceReady(&hi2c1, (dev_id << 1), 3, LSM9DS0_TIMEOUT) != HAL_OK) {
	}

	status = HAL_I2C_Mem_Read(&hi2c1, (dev_id << 1), reg_addr | LSM9DS0_MASK,
	I2C_MEMADD_SIZE_8BIT, reg_data, count, LSM9DS0_TIMEOUT);

	if (status != HAL_OK) {
		return DEVICE_ERROR;
	}
	return DEVICE_NORMAL;
}

/**
 * Wrapper for I2C write
 */
lsm9ds0_sensor_status memWrite(uint16_t dev_id, uint16_t reg_addr, uint8_t *reg_data,
		uint8_t count) {
	HAL_StatusTypeDef status = HAL_OK;

	while (HAL_I2C_IsDeviceReady(&hi2c1, (dev_id << 1), 3, LSM9DS0_TIMEOUT) != HAL_OK) {
	}

	status = HAL_I2C_Mem_Write(&hi2c1, (dev_id << 1), reg_addr | LSM9DS0_MASK,
	I2C_MEMADD_SIZE_8BIT, reg_data, count, LSM9DS0_TIMEOUT);
	//	TODO: Find a fix to remove this delay
	HAL_Delay(40);
	if (status != HAL_OK) {
		return DEVICE_ERROR;
	}
	return DEVICE_NORMAL;
}

/* Initialize LSM9DS0 for gyroscope */
lsm9ds0_sensor_status init_lsm9ds0_gyro(lsm9ds0_sensor *sensors) {
	uint8_t i2c_temp[2];
	/* Set to 0 all state values */
	memset(sensors->gyr_raw, 0, 3);
	memset(sensors->gyr, 0, 3);
	memset(sensors->gyr_prev, 0, 3);
	memset(sensors->calib_gyr, 0, 3);
	sensors->gyr_status = DEVICE_NORMAL;

	if (HAL_I2C_Mem_Read(&hi2c1, (GYRO_ADDR << 1), WHO_AM_I_G | LSM9DS0_MASK, 1,
			i2c_temp, 1, LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->gyr_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}
	if (i2c_temp[0] != GYRO_ID) {
		sensors->gyr_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	// 0x20 --> 0b01111111, 190Hz, LPF Cut off 70Hz
	// 0x21 --> 0b00100101, HPF cut off 0.45Hz
	// 0x22 --> 0b00000000
	// 0x23 --> 0b10000000, 245 DPS
	// 0x24 --> 0b00000000
	uint8_t GyroCTRreg[5] = { 0x7F, 0x25, 0x00, 0x80, 0x00 };
	if (HAL_I2C_Mem_Write(&hi2c1, (GYRO_ADDR << 1),
	CTRL_REG1_G | LSM9DS0_MASK, 1, GyroCTRreg, 5, LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->gyr_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	return sensors->gyr_status;

}

/* Calibration of lsm9ds0 gyroscope */
void calib_lsm9ds0_gyro(lsm9ds0_sensor *sensors) {

#if AUTO_GYRO_CALIB == 1
	int i = GYRO_N, d, cnt;
	cnt = 0;

	while (i--) {
		update_lsm9ds0_gyro(sensors);
		if (sensors->gyr_status == DEVICE_ERROR) {
			return;
		}
		cnt++;
		for (d = 0; d < 3; d++) {
			sensors->calib_gyr[d] += (float) sensors->gyr_raw[d];
			HAL_Delay(10);
		}
	}
	for (d = 0; d < 3; d++) {
		sensors->calib_gyr[d] /= (float) cnt;
	}
#else
	sensors->calib_gyr[0] = GYRO_OFFSET_X;
	sensors->calib_gyr[1] = GYRO_OFFSET_Y;
	sensors->calib_gyr[2] = GYRO_OFFSET_Z;
#endif
}

/* Update values for lsm9ds0 gyroscope */
lsm9ds0_sensor_status update_lsm9ds0_gyro(lsm9ds0_sensor *sensors) {
	int16_t rw_prev[3];

	rw_prev[0] = sensors->gyr_raw[0];
	rw_prev[1] = sensors->gyr_raw[1];
	rw_prev[2] = sensors->gyr_raw[2];
	/* IMU, Gyroscope measure */
	if (HAL_I2C_Mem_Read(&hi2c1, (GYRO_ADDR << 1), GYRO_VAL | LSM9DS0_MASK, 1,
			(uint8_t *) sensors->gyr_raw, 6,
			LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->gyr_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	if (sensors->gyr_raw[0] == rw_prev[0]
			&& sensors->gyr_raw[1] == rw_prev[1]
			&& sensors->gyr_raw[2] == rw_prev[2]) {
		sensors->gyr_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	sensors->gyr_status = DEVICE_NORMAL;

	sensors->gyr_prev[0] = sensors->gyr[0];
	sensors->gyr_prev[1] = sensors->gyr[1];
	sensors->gyr_prev[2] = sensors->gyr[2];

	sensors->gyr[0] =
			RAD(
					-((float) sensors->gyr_raw[2] - sensors->calib_gyr[2]) * GYRO_GAIN); // -Zg
	sensors->gyr[1] =
			RAD(
					-((float) sensors->gyr_raw[1] - sensors->calib_gyr[1]) * GYRO_GAIN); // -Yg
	sensors->gyr[2] =
			RAD(
					-((float) sensors->gyr_raw[0] - sensors->calib_gyr[0]) * GYRO_GAIN); // -Xb

	for (uint8_t i = 0; i < 3; i++) {
		sensors->gyr_f[i] = A_GYRO * sensors->gyr[i]
				+ (1 - A_GYRO) * sensors->gyr_prev[i];
	}

	return sensors->gyr_status;
}

/* Initialize LSM9DS0 for magnetometer */
lsm9ds0_sensor_status init_lsm9ds0_xm(lsm9ds0_sensor *sensors) {
	uint8_t i2c_temp[2];
	/* Set to 0 all state values */
	memset(sensors->xm_raw, 0, 3);
	memset(sensors->xm, 0, 3);
	memset(sensors->xm_prev, 0, 3);
	memset(sensors->xm_f, 0, 3);
	sensors->xm_norm = 0;
	sensors->xm_status = DEVICE_NORMAL;

	if (HAL_I2C_Mem_Read(&hi2c1, (XM_ADDR << 1), WHO_AM_I_XM | LSM9DS0_MASK, 1,
			i2c_temp, 1, LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->xm_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}
	if (i2c_temp[0] != XM_ID) {
		sensors->xm_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	// 0x1F --> 0b00000000
	// 0x20 --> 0b00001000
	// 0x21 --> 0b00000000
	// 0x22 --> 0b00000000
	// 0x23 --> 0b00000000
	// 0x24 --> 0b01110100, 100Hz Data rate, TEMP Enable
	// 0x25 --> 0b00000000, 2Gauss
	// 0x26 --> 0b00000000
	uint8_t XM_CTRreg[8] = { 0x00, 0x08, 0x00, 0x00, 0x0, 0x74, 0x00, 0x00 };

	if (HAL_I2C_Mem_Write(&hi2c1, (XM_ADDR << 1),
	XM_CTR_REG | LSM9DS0_MASK, 1, XM_CTRreg, 8, LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->xm_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	return sensors->xm_status;

}

/* Update values for lsm9ds0 magnetometer*/
lsm9ds0_sensor_status update_lsm9ds0_xm(lsm9ds0_sensor *sensors) {

	float offset[3] = { 0 };
	int16_t rw_prev[3];

	rw_prev[0] = sensors->xm_raw[0];
	rw_prev[1] = sensors->xm_raw[1];
	rw_prev[2] = sensors->xm_raw[2];

//	/* IMU, Magnetometer measure */
	if (HAL_I2C_Mem_Read(&hi2c1, (XM_ADDR << 1), XM_VAL | LSM9DS0_MASK, 1,
			(uint8_t *) sensors->xm_raw, 6, LSM9DS0_TIMEOUT) != HAL_OK) {
		sensors->xm_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	if (sensors->xm_raw[0] == rw_prev[0]
			&& sensors->xm_raw[1] == rw_prev[1]
			&& sensors->xm_raw[2] == rw_prev[2]) {
		sensors->xm_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	sensors->xm_status = DEVICE_NORMAL;

	sensors->xm_prev[0] = sensors->xm[0];
	sensors->xm_prev[1] = sensors->xm[1];
	sensors->xm_prev[2] = sensors->xm[2];

	offset[0] = ((float) (sensors->xm_raw[0]) - XM_OFFSET[0]);
	offset[1] = ((float) (sensors->xm_raw[1]) - XM_OFFSET[1]);
	offset[2] = ((float) (sensors->xm_raw[2]) - XM_OFFSET[2]);

	sensors->xm[0] = (offset[0] * XM_SCALE[6] + offset[1] * XM_SCALE[7]
			+ offset[2] * XM_SCALE[8]) * XM_GAIN; // Zxm
	sensors->xm[1] = -(offset[0] * XM_SCALE[3] + offset[1] * XM_SCALE[4]
			+ offset[2] * XM_SCALE[5]) * XM_GAIN; // -Yxm
	sensors->xm[2] = -(offset[0] * XM_SCALE[0] + offset[1] * XM_SCALE[1]
			+ offset[2] * XM_SCALE[2]) * XM_GAIN; // -Xxm

	for (uint8_t i = 0; i < 3; i++) {
		sensors->xm_f[i] = A_XM * sensors->xm[i]
				+ (1 - A_XM) * sensors->xm_prev[i];
	}

//	sensors->xm_norm = (float) norm(sensors->xm_f[0],
//			sensors->xm_f[1], sensors->xm_f[2]);

	return sensors->xm_status;
}

/**
 * Initialize accelerometer
 */
lsm9ds0_sensor_status init_lsm9ds0_acc(lsm9ds0_sensor *sensors) {
	uint8_t buffer =0 ;
	sensors->acc_status = DEVICE_NORMAL;

	sensors->acc_status = memRead(ACC_ADDR, WHO_AM_I_A, &buffer, 1);
	if (buffer != ACC_ID) {
		sensors->acc_status = DEVICE_ERROR;
		return DEVICE_ERROR;
	}

	// Set control register for bypass
	buffer = 0x00;
	sensors->acc_status = memWrite(ACC_ADDR, CTRL_REG0_XM, &buffer, 1);

	// Enable XYZ, rate 100Hz
	buffer = ACC_X_EN | ACC_Y_EN | ACC_Z_EN | LSM9DS0_ACCELDATARATE_100HZ | ACC_BDU;
	sensors->acc_status = memWrite(ACC_ADDR, CTRL_REG1_XM, &buffer, 1);

	// Set range
	buffer = A_ABW_773 | A_AST_NORMAL | LSM9DS0_ACCELRANGE_16G;
	sensors->acc_status = memWrite(ACC_ADDR, CTRL_REG2_XM, &buffer, 1);

	return sensors->acc_status;
}

/**
 * Update accelerometer values
 */
lsm9ds0_sensor_status update_lsm9ds0_acc(lsm9ds0_sensor *sensors) {
	uint8_t buffer[6];

	sensors->acc_status = memRead(ACC_ADDR, OUT_X_L_A, buffer, 6);
	if (sensors->acc_status == DEVICE_ERROR)
		return DEVICE_ERROR;

	sensors->acc_raw[0] = buffer[0] | (buffer[1]<<8);
	sensors->acc_raw[1] = buffer[2] | (buffer[3]<<8);
	sensors->acc_raw[2] = buffer[4] | (buffer[5]<<8);

	sensors->acc[0] = (float)sensors->acc_raw[0] * (ACC_FACTOR);
	sensors->acc[1] = (float)sensors->acc_raw[1] * (ACC_FACTOR);
	sensors->acc[2] = (float)sensors->acc_raw[2] * (ACC_FACTOR);
	return sensors->acc_status;
}

/**
 * Initialize temperature
 */
lsm9ds0_sensor_status init_lsm9ds0_temp(lsm9ds0_sensor *sensors) {
	uint8_t buffer;
	sensors->temp_status = memRead(TEMP_ADDR, CTRL_REG5_XM, &buffer, 1);

	buffer |= (1 << 7);
	sensors->temp_status = memWrite(TEMP_ADDR, CTRL_REG5_XM, &buffer, 1);

	return sensors->temp_status;
}

/**
 * Read temperature
 */
lsm9ds0_sensor_status update_lsm9ds0_temp(lsm9ds0_sensor *sensors) {
	uint8_t buffer[2];

	sensors->temp_status = memRead(TEMP_ADDR, OUT_TEMP_L_XM, buffer, 2);
	if (sensors->temp_status == DEVICE_ERROR)
		return DEVICE_ERROR;

	sensors->temp_raw = (buffer[1] << 8) | buffer[0];
	sensors->temp = 21.0 + (float) sensors->temp_raw / 8;
	return sensors->temp_status;
}

