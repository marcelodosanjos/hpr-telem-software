#include "rfm69hw.h"

/*uint8_t rfm69syncwords[] = { 0x0D, 0x06 , 0x06, 0x0D, 0x6, 0x6, 0x6, 0x6 };*/
uint8_t rfm69syncwords2[] = { 0x7A, 0x0E };
uint8_t rfmtx_id[] = { 0x6C, 0x73, 0x66, 0x72, 0x6B, 0x74 };
uint8_t rfm_cnt = 0x30;

/**
 * Set the MSBit of register 0x37 to '0' for fixed length
 * or to '1' for variable length packet.
 * Refer to datasheet page 72.
 * @param packeLength
 */
void SetPacketLength(PacketLength thePacketLength){

}

/**
 * Performs module reset without removing VDD from the module.
 * See RFM69HW datasheet v1.3, paragraph: 7.2.2, page: 76.
 */
void ManualModuleReset(){

//    HAL_GPIO_WritePin(RFM_SPI_PORTx, _RFMHW_RESET_Pin, GPIO_PIN_SET);
//    osDelay(1);
//    HAL_GPIO_WritePin(RFM_SPI_PORTx, _RFMHW_RESET_Pin, GPIO_PIN_RESET);
//    osDelay(5);
}

/**
 * Reads the currently stored value on the specified register.
 * @param regAddress, the register to read data from.
 * @return, returns the register's value.
 */
uint8_t ReadModuleRegister(uint8_t regAddress){

    uint8_t regVal = 0;
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi1, &regAddress, 1, 1000);
    HAL_SPI_Receive(&hspi1, &regVal, 1, 1000);
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOC, _SPI3_RFMHW_MISO_Pin,  )

    return regVal;
}

/**
 * Writes the specified register with the specified data.
 * @param regAddress, the address to write to.
 * @param regData, the data for writing.
 * @return for now, always zero (0).
 */
uint8_t WriteModuleRegister(uint8_t regAddress, uint8_t regData){

    uint8_t regRead = 0;
    regRead = (regAddress | 0b10000000);//to change the first bit to '1' that means 'write access to the register'
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi1, &regRead, 1, 1000);
    HAL_SPI_Transmit(&hspi1, &regData, 1, 1000);
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);

    return 0;

}

/**
 * Write N size data to
 * @param regAddress
 * @param regData
 * @param dataSize
 * @return
 */
uint8_t WriteModuleRegisterN(uint8_t regAddress, uint8_t *regData, uint8_t dataSize){

    uint8_t regRead = 0;
    regRead = (regAddress | 0b10000000);//to change the first bit to '1' that means 'write access to the register'
    regData[0]=regRead;
    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_RESET);

    HAL_SPI_Transmit(&hspi1, regData, dataSize, 100);

//    HAL_SPI_Transmit_DMA(&hspi1, regData, (uint16_t)dataSize);

//    HAL_SPI_Transmit_DMA(&hspi1, &regRead, 1);

    HAL_GPIO_WritePin(RFM_NSS_PIN_PORTx, RFM_SPI_NSS_PIN_NO, GPIO_PIN_SET);

    return 0;
}

void SetModulePreAmbleLenLSB(uint8_t preAmbleLen){

    WriteModuleRegister(REGPREAMBLELSB_REG, preAmbleLen);

}

void SetModulePreAmbleLenMSB(uint8_t preAmbleLen){

    WriteModuleRegister(REGPREAMBLEMSB_REG, preAmbleLen);

}

void SetModuleSyncWords(uint8_t *syncWordData, uint8_t howManySyncWords){

    uint8_t syncRegs[8] = { REGSYNCVALUE1_REG, REGSYNCVALUE2_REG, REGSYNCVALUE3_REG, REGSYNCVALUE4_REG,
                            REGSYNCVALUE5_REG, REGSYNCVALUE6_REG, REGSYNCVALUE7_REG, REGSYNCVALUE8_REG
                          };

    for(uint8_t i=0; i<howManySyncWords; i++){
        WriteModuleRegister(syncRegs[i], syncWordData[i]);
    }

}

void SetModuleBitRate(BitRate theBitRate){

    switch(theBitRate){
    case kbps1_2:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x68);
        WriteModuleRegister(REGBITRATELSB_REG, 0x2B);
        break;
    case kbps2_4:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x34);
        WriteModuleRegister(REGBITRATELSB_REG, 0x15);
        break;
    case kbps4_8:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x1A);
        WriteModuleRegister(REGBITRATELSB_REG, 0x0B);
        break;
    case kbps9_6:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x0D);
        WriteModuleRegister(REGBITRATELSB_REG, 0x05);
        break;
    case kbps19_2:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x06);
        WriteModuleRegister(REGBITRATELSB_REG, 0x83);
        break;
    case kbps38_4:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x03);
        WriteModuleRegister(REGBITRATELSB_REG, 0x41);
        break;
    case kbps76_8:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x01);
        WriteModuleRegister(REGBITRATELSB_REG, 0x1A);
        break;
    case kbps153_6:
        WriteModuleRegister(REGBITRATEMSB_REG, 0x00);
        WriteModuleRegister(REGBITRATELSB_REG, 0xDC);
        break;
    default:

        break;
    }

}

/**
 * Set module frequency deviation in Hz (from 0 - 16384 Hz).
 * @param freqDevHz
 */
void SetModuleFreqDeviation(uint16_t freqDevHz){

    uint16_t fdevhz = 0;
    fdevhz = freqDevHz / 61;
    uint8_t fdevmsb = fdevhz >> 8 ;
    uint8_t fdevlsb = fdevhz ;

    WriteModuleRegister(REGFDEVMSB_REG, fdevmsb);
    WriteModuleRegister(REGFDEVLSB_REG, fdevlsb);

}

/**
 * Call for module initialization.
 */
void InitializeModule(){

    //set node address
//    WriteModuleRegister(REGNODEADRS_REG, 0x99);

//    set module preamble to 3 bytes
//    SetModulePreAmbleLenMSB(0x00);
//    SetModulePreAmbleLenLSB(0x03);

//    //set module preamble to 8 bytes
    SetModulePreAmbleLenMSB(0x00);
    SetModulePreAmbleLenLSB(0x08);

    //enable syncword, sync size 3+1=4
//    WriteModuleRegister(REGSYNCCONFIG_REG, 0xD8);
//    SetModuleSyncWords(rfm69syncwords, 4);

    //enable syncword, sync size 1+1=2
    WriteModuleRegister(REGSYNCCONFIG_REG, 0xC8);
    SetModuleSyncWords(rfm69syncwords2, 2);

//    set freq. to 433mhz
    WriteModuleRegister(REGFRFMSB_REG, 0x6C);
    WriteModuleRegister(REGFRMMID_REG, 0x40);
    WriteModuleRegister(REGFRFLSB_REG, 0x00);
////    set freq. deviation (Hz)
//    SetModuleFreqDeviation(20000);
    //high power settings
    WriteModuleRegister(REGOCP_REG, 0xA); //over current protection off
    WriteModuleRegister(REGTESTPA1_REG, 0x5D);
    WriteModuleRegister(REGTESTPA2_REG, 0x7C);
    /*DO NOT UNCOMMENT*/
//    WriteModuleRegister(REGPALEVEL_REG, 0x7F); //pa01 pa02 on, not for rfmhw

//    WriteModuleRegister(REGPACKETCONFIG1_REG, 0x58); //set fixed packet length, whitening

    WriteModuleRegister(REGPACKETCONFIG1_REG, 0x18); //set fixed packet length, no whitening

    WriteModuleRegister(REGPAYLOADLENGTH_REG, 0x40); //set packet payload length to 64 bytes
//    WriteModuleRegister(REGPAYLOADLENGTH_REG, 0x41); //set packet payload length to 66 bytes
//    WriteModuleRegister(REGPAYLOADLENGTH_REG, 0x04); //set packet payload length to 4 bytes
//    WriteModuleRegister(REGPAYLOADLENGTH_REG, 0x7F); //set packet payload length to 127 bytes

    WriteModuleRegister(REGFIFOTHRESH_REG, 0x40); //fifo level exeeds, fifo level to 64 bytes
//    WriteModuleRegister(REGFIFOTHRESH_REG, 0x41); //fifo level exeeds, fifo level to 66 bytes
//    WriteModuleRegister(REGFIFOTHRESH_REG, 0x04); //fifo level exeeds, fifo level to 4 bytes
//    WriteModuleRegister(REGFIFOTHRESH_REG, 0x7F); //fifo level exeeds, fifo level to 127 bytes
    SetModuleBitRate(kbps9_6);

    //set module to sleep mode
    WriteModuleRegister(REGOPMODE_REG, 0x0);
    //set auto modes,
    WriteModuleRegister(REGAUTOMODES_REG, 0x5B);

}
